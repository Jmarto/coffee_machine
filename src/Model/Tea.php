<?php


namespace App\Model;


class Tea implements BeverageInterface
{
    public function getName(): string
    {
        return 'Tea';
    }

    public function getPrice(): int
    {
        return 3;
    }

}
