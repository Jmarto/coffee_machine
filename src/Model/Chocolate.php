<?php


namespace App\Model;


class Chocolate implements BeverageInterface
{
    public function getName(): string
    {
        return  'Chocolate';
    }

    public function getPrice(): int
    {
        return 5;
    }

}
