<?php


namespace App\Model;

interface BeverageInterface
{
    /**
     * @return string
     */

    public function getName(): string;
    /**
     * @return int
     */
    public function getPrice(): int;

}
