<?php


namespace App\Model;


class Coffee implements BeverageInterface
{
    public function getName(): string
    {
        return 'Coffee';
    }

    public function getPrice(): int
    {
        return 2;
    }


}
