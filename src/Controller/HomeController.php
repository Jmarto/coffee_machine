<?php

namespace App\Controller;

use App\Entity\CoffeeMachine;
use App\Handler\BeverageHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


class HomeController extends Controller
{
    /**
     * @var BeverageHandler
     */
    private $beverageHandler;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * HomeController constructor.
     * @param BeverageHandler $beverageHandler
     */
    public function __construct(BeverageHandler $beverageHandler)
    {
        $this->beverageHandler = $beverageHandler;
    }


    /**
     * @Route("/", name="home")
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return  Response
     */
    public function baseAction(Request $request, SerializerInterface $serializer): Response
    {
        if ($request->isXmlHttpRequest() && $request->isMethod('POST')) {
            $formData = $request->request->all();
            $creditLeft = $this->sumPayment((int)$formData['coffee_machine_current_credit'], $formData['coffee_machine']['beverageName']);

            if ($creditLeft >= 0) {
                $coffeeMachine = $serializer->deserialize(json_encode($formData['coffee_machine']), CoffeeMachine::class, 'json');
                $this->getDoctrine()->getManager()->persist($coffeeMachine);
                $this->getDoctrine()->getManager()->flush();
                return new JsonResponse(['creditLeft' => $creditLeft]);
            }

            return new JsonResponse(['message' => 'Not enough credits'], Response::HTTP_BAD_REQUEST);
        }

        return $this->render('home/base.html.twig', [
            'beverages' => $this->beverageHandler->getBeverages(),
        ]);
    }


    public function sumPayment(int $currentCredit, string $beverageName): int
    {
        $beverage = $this->beverageHandler->getBeverageByName($beverageName);
        return $currentCredit - $beverage->getPrice();
    }
}
