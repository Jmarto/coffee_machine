<?php


namespace App\Handler;


use App\Model\BeverageInterface;

class BeverageHandler
{

    /**
     * @var iterable
     */
    private $beverages;

    /**
     * BeverageHandler constructor.
     * @param iterable $beverages
     */
    public function __construct(iterable $beverages)
    {
        $this->beverages = $beverages;
    }


    public function getBeverages(): iterable
    {
        return $this->beverages;
    }

    public function getBeverageByName(string $nameBeverage)
    {
        /** @var BeverageInterface $beverage */
        foreach ($this->beverages as $beverage) {
            if ($beverage->getName() === $nameBeverage) {
                return $beverage;
            }
        }
        throw new \InvalidArgumentException($nameBeverage . ' Class not exist');
    }
}
