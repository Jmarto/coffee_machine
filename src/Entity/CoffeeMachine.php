<?php


namespace App\Entity;

use App\Model\BeverageInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 */
class CoffeeMachine
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotNull
     * @var string $beverageName
     */
    private $beverageName;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 0,
     *      max = 4,
     * )
     * @var int $milkLevel
     */
    private $milkLevel = 0;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 0,
     *      max = 4,
     * )
     * @var int $sugarLevel
     */
    private $sugarLevel = 0;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBeverageName(): string
    {
        return $this->beverageName;
    }


    /**
     * @param string $beverageName
     * @return CoffeeMachine
     */
    public function setBeverageName(string $beverageName): CoffeeMachine
    {
        $this->beverageName = $beverageName;

        return $this;
    }

    /**
     * @return int
     */
    public function getMilkLevel(): int
    {
        return $this->milkLevel;
    }


    /**
     * @param int $milkLevel
     * @return CoffeeMachine
     */
    public function setMilkLevel(int $milkLevel): CoffeeMachine
    {
        $this->milkLevel = $milkLevel;

        return $this;
    }

    /**
     * @return int
     */
    public function getSugarLevel(): int
    {
        return $this->sugarLevel;
    }


    /**
     * @param int $sugarLevel
     * @return CoffeeMachine
     */
    public function setSugarLevel(int $sugarLevel): CoffeeMachine
    {
        $this->sugarLevel = $sugarLevel;

        return $this;
    }


}
