# Coffee Machine ! 

Well hi there! This repository holds the code and script
for this Ermeo Test :

﻿Nous aurions besoin d'une nouvelle machine a café. Pour cela, nous aurions besoin de coder la machine.


Les contraintes de construction de la machine à café sont les suivantes :


- POO uniquement

- Utilisation d'un design pattern pour structurer le fonctionnement de la machine. Les pattern Factory et Singleton sont interdits.

- Un seul objet d'entré pour manipuler la machine, l'objet "CoffeeMachine.php"



Objectif du test : évaluer la modélisation et la propreté du code.



Spécifications de la machine :

 
La machine à café doit pouvoir implémenter les fonctionnalités suivantes :


- Un bouton café qui retourne une classe "Coffee". Prix : 2 pièces de monnaie

- Un bouton thé qui retourne une classe "Tea". Prix : 3 pièces de monnaie

- Un bouton chocolat qui retourne une classe "Chocolate". Prix : 5 pièces de monnaie

- Un bouton pour choisir le niveau de sucre entre 0 et 4 qui doit être présent dans la boisson finale

- Un bouton pour choisir le niveau de lait entre 0 et 4 qui doit être présent dans la boisson finale

- Une fente pour ajouter de l'argent

- L'affichage de la somme actuelle

- Un bouton pour récupérer la totalité de la somme

## Setup

If you've just downloaded the code, congratulations!!

To get it working, follow these steps:

**Download Composer dependencies**

Make sure you have [Composer installed](https://getcomposer.org/download/)
and then run:

```
composer install
```

You may alternatively need to run `php composer.phar install`, depending
on how you installed Composer.

**Lunch the Database with Docker**

Make sure you have Docker Installed
and then run:

```
docker-compose up -d
```

**Configure the .env (or .env.local) File**

Open the `.env` file and make any adjustments you need - specifically
`DATABASE_URL`. Or, if you want, you can create a `.env.local` file
and *override* any configuration you need there (instead of changing
`.env` directly).

**Start the built-in web server**

You can use Nginx or Apache, but Symfony's local web server
works even better.

To install the Symfony local web server, follow
"Downloading the Symfony client" instructions found
here: https://symfony.com/download - you only need to do this
once on your system.

Then, to start the web server, open a terminal, move into the
project, and run:

```
symfony serve
```

(If this is your first time using this command, you may see an
error that you need to run `symfony server:ca:install` first).

Now check out the site at `https://localhost:8000`

Have fun!

**Setup the Database**

Again, make sure `.env` is setup for your computer. Then, create
the database & tables!

```
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
```

If you get an error that the database exists, that should
be ok. But if you have problems, completely drop the
database (`doctrine:database:drop --force`) and try again.


## Thanks!
